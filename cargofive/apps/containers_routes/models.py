from django.db import models

# Create your models here.

#Modelos necesarios para el challenge con sus atributos

class Contracts(models.Model):
    name = models.CharField("Nombre", max_length=200)
    date = models.DateField("Fecha")

    def __str__(self):
        return f'{self.name}'


class Rates(models.Model):
    origin = models.CharField("Puerto de origen", max_length=200)
    destination = models.CharField("Puerto de destino", max_length=200)
    currency = models.CharField("Moneda", max_length=200)
    twenty = models.CharField("20'GP", max_length=200)
    forty = models.CharField("40'GP", max_length=200)
    fortyhc = models.CharField("40'HC", max_length=200)
    contracts = models.ForeignKey(Contracts, on_delete=models.CASCADE)


class Files(models.Model):
    file = models.FileField(upload_to='', null=True)
    created_at = models.DateField(auto_now_add=True)

    def __str__(self):
        return f'{self.file}'