# Generated by Django 3.2.12 on 2022-02-07 00:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('containers_routes', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='rates',
            name='contracts',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='containers_routes.contracts'),
            preserve_default=False,
        ),
    ]
