# Generated by Django 3.2.12 on 2022-02-08 00:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('containers_routes', '0011_auto_20220207_2147'),
    ]

    operations = [
        migrations.AddField(
            model_name='files',
            name='created_at',
            field=models.DateField(auto_now_add=True, default='2022-01-01'),
            preserve_default=False,
        ),
    ]
