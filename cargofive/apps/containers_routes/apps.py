from django.apps import AppConfig


class ContainersRoutesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.containers_routes'
