from django import forms

class DateInput(forms.DateInput):
    input_type = 'date'


class LoadForm(forms.Form):
    #Formulario para carga
    name = forms.fields.CharField(label='Nombre', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    date = forms.fields.CharField(label='Fecha', required=True, widget=DateInput(attrs={'class': 'form-control'}))
    file = forms.fields.FileField(label='Archivo', required=True, widget=forms.FileInput(attrs={'class': 'form-control'}))

class CompareForm(forms.Form):
    #Formulario para Comparar
    pass