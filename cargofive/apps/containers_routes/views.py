from django.shortcuts import render
from django.views.generic import FormView
from .forms import LoadForm, CompareForm
from .models import Contracts, Rates, Files
import json

import pandas as pd

# Create your views here.


class LoadDataView(FormView):
    template_name = "containers_routes/data_load.html"
    form_class = LoadForm
    form = form_class

    #Función post que ejecutará el codigo al hacer click en boton tomando posteriormente los datos de los input del form 

    def post(self, request, *args, **kwargs):
        #Llamamos al formulario que se renderiza en las templates y evitamos los errores que vienen por defecto en el formulario
        form = self.form(request.POST or None, request.FILES or None)

        #llamamos al contexto
        context = self.get_context_data(*args, **kwargs)

        #Obtengo el archivo cargado y enviado por formulario
        file = self.request.FILES.get('file')

        #Divido el nombre del archivo para obtener la extension xlsx, separo el nombre por el '.', archivo.xlsx dividido queda archivo y xlsx
        extension = file.name.split('.')

        #Evalúo la extensión obtenida, esta se encuentra en la posicion 1 de extension, osea extension[1] = xlsx
        # si es xlsx, osea excel, no entramos al bucle, caso contrario, si el archivo cargado no es excel, entramos al condicional if y le inyectamos un error al form
        if extension[1] != 'xlsx':
            #Inyección de error, gracias a esta parte, evitamos entrar al bloque is_valid() ya que le inyectamos el error
            form.add_error('file', 'Solo se aceptan archivos Excel')

        #Si llegamos a esta parte con el formulario correctamente validado, y sin errores, ejecutaremos el codigo de guardado
        if form.is_valid():

            #Creamos un objeto File con el archivo obtenido, esto nos servirá para alojar en bbdd la ruta de los archivos que cargamos
            #y con los cuales trabajaremos en el punto dos

            Files.objects.create(file=file)

            #Obtenemos name y date de los input del formulario

            name = self.request.POST.get('name')
            date = self.request.POST.get('date')

            #Creamos un registro Contracts en bbdd con los datos name y date
            contracts = Contracts.objects.create(name=name, date=date)

            #Obtenemos su id
            contract = Contracts.objects.get(id=contracts.id)

            #Leemos el excel con pandas, transformado a dataframe
            df = pd.read_excel(
                file, usecols=['POL', 'POD', 'Curr.', "20'GP", "40'GP", "40'HC"])

            #Recorremos cada fila y con los datos de sus celdas ( POL,POD,etc ) + el registro contract recien creado, damos de alta las RATES
            for i in df.index:
                #Aqui las creamos
                Rates.objects.create(
                    origin=df["POL"][i], destination=df["POD"][i], currency=df["Curr."][i], twenty=str(df["20\'GP"][i]), forty=str(df["40\'GP"][i]), fortyhc=str(df["40\'HC"][i]), contracts=contract)
            
            #Pasamos un str success que se mostrará en pantalla cuando el formulario se cargue correctamente
            success = "Formulario cargado Correctamente"
            context['success'] = success

            #Creamos un queryset con la lista de rates filtradas por contract (Obtenemos los registros creados mas arriba) y lo pasamos por context
            rates_list=Rates.objects.filter(contracts=contract)
            context['rates_list']=rates_list
            
            #Pasamos el contract por contexto
            context['contracts'] = contract

            #Devolvemos a la misma página el contexto con el success y la rates_list
            return self.render_to_response(context=context)
        else:

            #Pasamos por contexto el formulario con errores si no es válido
            context['form'] = form

        #Devolvemos a la misma página el contexto con el form con errores
        return self.render_to_response(context=context)


class CompareDataView(FormView):
    template_name = 'containers_routes/compare_data.html'
    form_class = CompareForm


    #Funcion post que se lanza al enviar form
    def post(self, request, *args, **kwargs):

        #Obtenemos el contexto

        context = self.get_context_data(*args, **kwargs)

        #Obtenemos los ultimos dos archivos del histórico en bbdd

        files = Files.objects.all().order_by('-created_at')[:2]

        #Leemos como dataframe ambos files con las columnas específicas

        first_file = pd.read_excel(files[0].file, usecols=[
                                   'POL', 'POD', 'Routing'])
        second_file = pd.read_excel(files[1].file, usecols=[
                                    'POL', 'POD', 'Routing'])

        #Hacemos merge en ambos dataframe y con how "left" obtenemos las filas con diferencias entre un dataframe y otro y las que son idénticas entre los mismos son marcadas con both                          

        df_primary = first_file.merge(
            second_file, how='left', indicator='union')
        df_secondary = second_file.merge(
            first_file, how='left', indicator='union')

        #Quitamos las filas identicas con drop, no son necesarias ya que necesitamos las diferencias. Quitamos las que son both    

        primary_indexNames = df_primary[df_primary['union'] == 'both'].index
        df_primary.drop(primary_indexNames, inplace=True)

        secondary_indexNames = df_secondary[df_secondary['union']
                                            == 'both'].index
        df_secondary.drop(secondary_indexNames, inplace=True)

        #Transformamos los dataframe a json

        json_records = df_primary.reset_index().to_json(orient='records')
        first = []
        first = json.loads(json_records)

        json_records = df_secondary.reset_index().to_json(orient='records')
        second = []
        second = json.loads(json_records)

        #Enviamos por context los json, un data que servira de bandera para mostrar o no la información de los json y la cantidad 
        #de registros que cambiaron en cada archivo

        context['first'] = first
        context['second'] = second
        context['data'] = True
        context['first_quantity'] = len(first)
        context['second_quantity'] = len(second)

        return self.render_to_response(context=context)
